declare module '@sgtools/console' {
    interface AppConsole {
        nano(text: string): string;
        nanoFile(path: string): boolean;
    }
}
export {};
