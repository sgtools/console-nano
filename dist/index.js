"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const tmp = require("tmp");
const child_process_1 = require("child_process");
const console_1 = require("@sgtools/console");
console_1.AppConsole.prototype.nano = (text) => {
    let tmpFile = tmp.fileSync();
    fs.writeFileSync(tmpFile.name, text);
    child_process_1.spawnSync('nano', [tmpFile.name], { stdio: ['inherit', 'inherit', 'inherit'] });
    let newtext = fs.readFileSync(tmpFile.name).toString();
    tmpFile.removeCallback();
    return newtext;
};
console_1.AppConsole.prototype.nanoFile = (path) => {
    if (!fs.existsSync(path))
        return false;
    child_process_1.spawnSync('nano', [path], { stdio: ['inherit', 'inherit', 'inherit'] });
    return true;
};
