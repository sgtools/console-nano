import * as fs from 'fs';
import * as tmp from 'tmp';
import { spawnSync } from 'child_process';

import { AppConsole } from '@sgtools/console';

declare module '@sgtools/console'
{
    export interface AppConsole
    {
        nano(text: string): string;
        nanoFile(path: string): boolean;
    }
}

AppConsole.prototype.nano = (text: string) =>
{
    let tmpFile = tmp.fileSync();
    fs.writeFileSync(tmpFile.name, text);
    spawnSync('nano', [tmpFile.name], { stdio: ['inherit', 'inherit', 'inherit'] });
    let newtext = fs.readFileSync(tmpFile.name).toString();
    tmpFile.removeCallback();
    return newtext;
}

AppConsole.prototype.nanoFile = (path: string) =>
{
    if (!fs.existsSync(path)) return false;

    spawnSync('nano', [path], { stdio: ['inherit', 'inherit', 'inherit'] });
    return true;
}